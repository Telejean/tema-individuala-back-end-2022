const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS pasageri_inscrisi(nume VARCHAR(255), prenume  VARCHAR(255), telefon  VARCHAR(255) , email  VARCHAR(255),data_plecare VARCHAR(10), data_intoarcere VARCHAR(10), destinatie VARCHAR(255), facebook VARCHAR(255),cod_bilet VARCHAR(255))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/pasager", (req, res) => {
  let pasager = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    email: req.body.email,
  };

  let error = [];
  //aici rezolvati cerintele (づ｡◕‿‿◕｡)づ


  if (error.length === 0) {

    const sql = `INSERT INTO pasageri_inscrisi (
      nume,
      prenume,
      telefon,
      email,
      data_plecare,
      data_intoarcere,
      destinatie,
      facebook,
      cod_bilet,
      cnp,
      varsta, 
      durata_calatoriei,
      acord_parental
      gen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        pasager.nume,
        pasager.prenume,
        pasager.telefon,
        pasager.email,
        pasager.data_plecare,
        pasager.data_intoarcere,
        pasager.destinatie,
        pasager.facebook,
        pasager.cod_bilet
      ],
      function (err, result) {
        if (err) throw err;
        console.log("pasager creat cu succes!");
        res.status(200).send({
          message: "pasager creat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("pasagerul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
//modifica si din front la index.html
